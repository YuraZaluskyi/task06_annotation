package com.zaluskyi;

/*
* Read recommended sources.
Create your own annotation. Create class with a few fields, some of which annotate with this annotation.
Through reflection print those fields in the class that were annotate by this annotation.
Print annotation value into console (e.g. @Annotation(name = "111"))
Invoke method (three method with different parameters and return types)
Set value into field not knowing its type.
Invoke myMethod(String a, int ... args) and myMethod(String … args).
Create your own class that received object of unknown type and show all information about that Class.

* */

import com.zaluskyi.view.View;

public class Main {
    public static void main(String[] args) {
        View myView = new View();
        myView.printFields();
        System.out.println("");
        myView.printMethods();

    }
}
