package com.zaluskyi.view;

import com.zaluskyi.annotation.CustomerAnnotation;
import com.zaluskyi.model.Commodity;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class View {

    public void printFields() {
        System.out.println("Print fields with annotation");
        Class clazz = Commodity.class;
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(CustomerAnnotation.class)) {
                CustomerAnnotation customerAnnotation = field.getAnnotation(CustomerAnnotation.class);
                String title = customerAnnotation.title();
                int price = customerAnnotation.price();
                int quantity = customerAnnotation.quantity();
                System.out.println("field ->" + field.getName());
                System.out.println("title in @CustomerAnnotation -> " + title);
                System.out.println("price in @CustomerAnnotation -> " + price);
                System.out.println("quantity in @CustomerAnnotation -> " + quantity);
            }
        }
    }

    public void printMethods() {
        System.out.println("Print methods");
        Class clazz = Commodity.class;
        Method[] methods = clazz.getDeclaredMethods();
        for (Method method : methods) {
            System.out.println("method's name -> " + method.getName());
            System.out.println("method's return type" + method.getReturnType());
            System.out.println("");
        }
    }


}
