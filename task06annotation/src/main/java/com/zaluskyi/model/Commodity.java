package com.zaluskyi.model;

import com.zaluskyi.annotation.CustomerAnnotation;

public class Commodity {
    @CustomerAnnotation(title = "customer brand", quantity = 10)
    private String title;
    private int price;
    @CustomerAnnotation(quantity = 123)
    private int quantity;

    public Commodity() {
    }

    public Commodity(String title, int price, int quantity) {
        this.title = title;
        this.price = price;
        this.quantity = quantity;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Commodity commodity = (Commodity) o;

        if (price != commodity.price) return false;
        if (quantity != commodity.quantity) return false;
        return title != null ? title.equals(commodity.title) : commodity.title == null;
    }

    @Override
    public int hashCode() {
        int result = title != null ? title.hashCode() : 0;
        result = 31 * result + price;
        result = 31 * result + quantity;
        return result;
    }

    @Override
    public String toString() {
        return "Commodity{" +
                "title='" + title + '\'' +
                ", price=" + price +
                ", quantity=" + quantity +
                '}';
    }
}
